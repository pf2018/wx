package com.pf.wx.mp.controller;

import java.io.UnsupportedEncodingException;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.alibaba.fastjson.JSONObject;
import com.pf.wx.mp.config.WxMpConfiguration;
import com.pf.wx.mp.utils.EmojiFilter;
import com.pf.wx.mp.utils.RestTemplateHelper;

import me.chanjar.weixin.common.api.WxConsts;
import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.bean.result.WxMpOAuth2AccessToken;
import me.chanjar.weixin.mp.bean.result.WxMpUser;

/**
 * @author 付青建
 * @create 2018-09-01 上午11:56
 * @desc
 **/
//@RestController
@Controller
@RequestMapping("/api/bbs/wx/authuser/{appid}")
public class WxAuthUserController {


//    public static final String PF_DOMAINURL = "http://bbsm.scxcsj.com";
//    public static final String PF_DOMAINURL = "http://pfm.enjoysala.top";
//    public static final String WX_DOMAINURL = "http://wx.scxcsj.com";
//    public static final String WX_DOMAINURL = "http://weixin.enjoysala.top";
    
	
	@Value("${server.port}")
	protected String prot;
	
	@Value("${domain.wx}")
	protected String wx_domainurl;

	@Value("${domain.pf}")
	protected String pf_domainurl;

    private Map<String,String> codeMap = new HashMap<String,String>();

//	public static void main(String[] args) throws UnsupportedEncodingException {
//		String callback = "aHR0cDovL2xvY2FsaG9zdDo4MjgwL3VzZXJDZW50ZXI_dG93bl9pZD01MTExMzIwMQ==";
//		String encoder = Base64.getUrlEncoder().encodeToString(callback.getBytes());
//		System.out.println("aHR0cDovL2xvY2FsaG9zdDo4MjgwL3VzZXJDZW50ZXI_dG93bl9pZD01MTExMzIwMQ==");
//		System.out.println(encoder);
//		encoder = "aHR0cDovL2xvY2FsaG9zdDo4MjgwL3VzZXJDZW50ZXI_dG93bl9pZD01MTExMzIwMQ==";
//		String decoder = new String(Base64.getUrlDecoder().decode(encoder),"UTF-8");
//		System.out.println(decoder);
//	}

    @GetMapping("/getUserInfoByAuth")
    public String getAuthUrlAndUserInfo(@PathVariable String appid, String townId, String callback,String openid) {
        WxMpService wxMpService = WxMpConfiguration.getMpServices().get(appid);
        
        String redirectUrl = wx_domainurl + "/api/bbs/wx/authuser/" + appid + "/getUserInfo?townId=" + townId;
        if (StringUtils.isNoneEmpty(openid)) {
            redirectUrl += "&openid="+openid;
        }
        if (StringUtils.isNoneEmpty(callback)) {
            redirectUrl += "&callback=" + Base64.getUrlEncoder().encodeToString(callback.getBytes());
        }
        System.out.println("*******************");
        System.out.println(redirectUrl);
        System.out.println("wxMpService="+wxMpService);
        System.out.println("*******************");
        String authUrl = wxMpService.oauth2buildAuthorizationUrl(redirectUrl, WxConsts.OAuth2Scope.SNSAPI_USERINFO, null);
        return "redirect:" + authUrl;
    }

    @GetMapping("/getOpenId")
    public String getAuthUrlAndOpenId(@PathVariable String appid, String townId, String callback) {
        WxMpService wxMpService = WxMpConfiguration.getMpServices().get(appid);
        String redirectUrl = wx_domainurl + "/api/bbs/wx/authuser/"
                + appid
                + "/getUserInfo?townId=" + townId
                + "&t=" + System.currentTimeMillis();

        if (StringUtils.isNoneEmpty(callback)) {
            redirectUrl += "&callback=" + Base64.getUrlEncoder().encodeToString(callback.getBytes());
        }

        String authUrl = wxMpService.oauth2buildAuthorizationUrl(redirectUrl, WxConsts.OAuth2Scope.SNSAPI_BASE, null);
        System.out.println("1---------------1");
        System.out.println(authUrl);
        System.out.println("1---------------1");
        return "redirect:" + authUrl;
    }

    @GetMapping("/getUserInfo")
    public String getUserInfobyCode(@PathVariable String appid, String code, String townId, String callback,String openid,HttpServletResponse response) throws WxErrorException {
        String nikeName = "";
        String weixinHeader = "";
	    if(codeMap.containsKey(code)){
            openid = codeMap.get(code);
        } else {
            WxMpService wxMpService = WxMpConfiguration.getMpServices().get(appid);
            WxMpOAuth2AccessToken wxMpOAuth2AccessToken = wxMpService.oauth2getAccessToken(code);
            WxMpUser wxMpUser = wxMpService.oauth2getUserInfo(wxMpOAuth2AccessToken, null);
            if(EmojiFilter.containsEmoji(wxMpUser.getNickname())) {
                wxMpUser.setNickname(EmojiFilter.filterEmoji(wxMpUser.getNickname()));
            }
            nikeName = Base64.getUrlEncoder().encodeToString(wxMpUser.getNickname().getBytes());
            openid = StringUtils.isNotBlank(openid)?openid:wxMpUser.getOpenId();
            weixinHeader = wxMpUser.getHeadImgUrl();
            codeMap.put(code, openid);
        }


//        JSONObject param = new JSONObject();
//        param.put("openId", StringUtils.isNotBlank(openid)?openid:wxMpUser.getOpenId());
//        param.put("weixinHeader", wxMpUser.getHeadImgUrl());
//        param.put("nickName", nikeName);
//        param.put("t", System.currentTimeMillis());

        String paramStr = "openId=" + openid +
                "&weixinHeader=" + weixinHeader +
                "&nickName=" + nikeName +
                "&appid="+ appid +
                "&t=" + System.currentTimeMillis();
        if (!StringUtils.isEmpty(callback)) {
            // 此处的callback 经历了一次base64编码，需要在解析
            // 解析重新组装callbackurl
            try {
                callback = new String(Base64.getUrlDecoder().decode(callback),"UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
                callback = "";
            }
            if (callback.indexOf("?") > -1) {
                callback += "&openid=" + openid;
                if (callback.indexOf("town_id") == -1) {
                    callback += "&town_id=" + townId;
                }
            } else {
                callback += "?openid=" + openid;
                if (callback.indexOf("town_id") == -1) {
                    callback += "&town_id=" + townId;
                }
            }
//            param.put("callback", Base64.getUrlEncoder().encodeToString(callback.getBytes()));
//            paramStr += "&callback=" + Base64.getUrlEncoder().encodeToString(callback.getBytes());
        }
        String url  = pf_domainurl + "/api/bbs/wx/user/saveWxUserInfo/" + townId +"?" +paramStr;
        System.out.println("================");
        System.out.println(url);
        System.out.println("callback="+callback);
        System.out.println("================");
        RestTemplateHelper.getForJSONObject(url, new JSONObject());
        
        if (StringUtils.isNotEmpty(callback)) {
//                response.sendRedirect(callback);
            return "redirect:" + callback;
        } else {
//                response.sendRedirect(pf_domainurl + "/index?town_id=" + townId + "&openid=" + param.getString("openId") +"&t="+System.currentTimeMillis());
            return "redirect:"+pf_domainurl + "/index?town_id=" + townId + "&openid=" + openid +"&t="+System.currentTimeMillis();
        }
//        return "redirect:" + PF_DOMAINURL + "/api/bbs/wx/user/saveWxUserInfo/" + townId + "?" + paramStr;
//        return "success";
    }
}
