package com.pf.wx.mp.model;

import lombok.Data;
import me.chanjar.weixin.mp.bean.result.WxMpUser;

/**
 * @author 付青建
 * @create 2018-09-12 下午11:27
 * @desc
 **/
@Data
public class WxpfUser extends WxMpUser {
    private String townId;
}
