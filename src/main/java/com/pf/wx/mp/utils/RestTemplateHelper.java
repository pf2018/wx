package com.pf.wx.mp.utils;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

public class RestTemplateHelper {
	
	public static String postForObject(String url,String params) {
		RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        MediaType type = MediaType.parseMediaType("application/json; charset=UTF-8");
        headers.setContentType(type);
        headers.add("Accept", MediaType.APPLICATION_JSON.toString());
        
        HttpEntity<String> formEntity = new HttpEntity<String>(params, headers);

        String result = restTemplate.postForObject(url, formEntity, String.class);
		return result;
	}
	
	public static String getForJSONObject (String url,JSONObject param) {
		RestTemplate restTemplate = new RestTemplate();
//        HttpHeaders headers = new HttpHeaders();
//        MediaType type = MediaType.parseMediaType("application/json; charset=UTF-8");
//        headers.setContentType(type);
//        headers.add("Accept", MediaType.APPLICATION_JSON.toString());
        restTemplate.getForObject(url, String.class, param);
		return restTemplate.getForObject(url, String.class, param);
	}
	
//	public static void main(String[] args) {
//		String url = "http://pfm.enjoysala.top/api/bbs/wx/user/saveWxUserInfo/51113201?openId=999999";
//		System.out.println(RestTemplateHelper.getForJSONObject(url,new JSONObject()));
//	}
	
	/**
	 * body 为{}
	 * @param url
	 * @param body
	 * @return
	 */
	public static String postForJsonObject(String url,JSONObject body) {
		return postForObject(url,body.toString());
	}
	
	/**
	 * body 为[{},{}]
	 * @param url
	 * @param body
	 * @return
	 */
	public static String postForJsonArray(String url,JSONArray body) {

		return postForObject(url,body.toString());
	}
	
	public static String deleteForObject(String url,Map<String, ?> uriVariables,String body) {
		RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        MediaType type = MediaType.parseMediaType("application/json; charset=UTF-8");
        headers.setContentType(type);
        headers.add("Accept", MediaType.APPLICATION_JSON.toString());

        URI uri = restTemplate.getUriTemplateHandler().expand(url, uriVariables);
        HttpMethod method = HttpMethod.DELETE;
        HttpEntity<String> formEntity = new HttpEntity<String>(body, headers);

        ResponseEntity<String> result = restTemplate.exchange(uri, method, formEntity, String.class);
        
		return result.getBody();
	}
	
	
	public static String deleteForJsonObject(String url,JSONObject body) {
		return deleteForObject(url,new HashMap<String, String>(),body.toString());
	}
	
	public static String deleteForJsonArray(String url,JSONArray body) {
		return deleteForObject(url,new HashMap<String, String>(),body.toString());
	}
}
