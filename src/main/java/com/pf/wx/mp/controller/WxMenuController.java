package com.pf.wx.mp.controller;

import com.pf.wx.mp.config.WxMpConfiguration;
import me.chanjar.weixin.common.bean.menu.WxMenu;
import me.chanjar.weixin.common.bean.menu.WxMenuButton;
import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.mp.bean.menu.WxMpGetSelfMenuInfoResult;
import me.chanjar.weixin.mp.bean.menu.WxMpMenu;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import static me.chanjar.weixin.common.api.WxConsts.MenuButtonType;

/**
 * @author Binary Wang(https://github.com/binarywang)
 */
@RestController
@RequestMapping("/api/bbs/wx/menu/{appid}")
public class WxMenuController {

	@Value("${domain.wx}")
	protected String wx_domainurl;

	@Value("${domain.pf}")
	protected String pf_domainurl;
    /**
     * <pre>
     * 自定义菜单创建接口
     * 详情请见：https://mp.weixin.qq.com/wiki?t=resource/res_main&id=mp1421141013&token=&lang=zh_CN
     * 如果要创建个性化菜单，请设置matchrule属性
     * 详情请见：https://mp.weixin.qq.com/wiki?t=resource/res_main&id=mp1455782296&token=&lang=zh_CN
     * </pre>
     *
     {
         "buttons": [{
             "type": "view",
             "name": "五彩岷东",
             "url": "https://mp.weixin.qq.com/mp/profile_ext?action=home&__biz=MzUxNzAzMzAxNg==#wechat_redirect",
             "sub_button": []
         }, {
             "type": "view",
             "name": "乡村视界",
             "url": "http://wx.scxcsj.com/api/bbs/wx/authuser/wx271742f01bd70cc3/getUserInfoByAuth?townId=511322",
             "sub_button": []
         }]
     }
     *
     * @return 如果是个性化菜单，则返回menuid，否则返回null
     */
    @PostMapping("/create")
    public String menuCreate(@PathVariable String appid, @RequestBody WxMenu menu) throws WxErrorException {
        return WxMpConfiguration.getMpServices().get(appid).getMenuService().menuCreate(menu);
    }

    @GetMapping("/create")
    public String menuCreateSample(@PathVariable String appid) throws WxErrorException {
//        WxMenu menu = new WxMenu();
//        WxMenuButton button1 = new WxMenuButton();
//        button1.setType(MenuButtonType.VIEW);
//        button1.setName("乡村生活");
//        button1.setUrl("http://weixin.enjoysala.top/api/bbs/wx/authuser/wx8c6976c9b7ec638b/getUserInfoByAuth?townId=511322");
//        WxMenuButton button2 = new WxMenuButton();
//        button2.setType(MenuButtonType.VIEW);
//        button2.setName("团购商城");
//        button2.setUrl("http://sxy.lsqxd.net/app/index.php?i=13&c=entry&eid=265");
//        WxMenuButton button3 = new WxMenuButton();
//        button3.setName("便民服务");

//        WxMenuButton button2 = new WxMenuButton();
//        button2.setType(WxConsts.BUTTON_MINIPROGRAM);
//        button2.setName("小程序");
//        button2.setAppId("wx286b93c14bbf93aa");
//        button2.setPagePath("pages/lunar/index.html");
//        button2.setUrl("http://mp.weixin.qq.com");

//        WxMenuButton button3 = new WxMenuButton();
//        button3.setName("菜单");

//        menu.getButtons().add(button1);
//        menu.getButtons().add(button2);
//        menu.getButtons().add(button3);
//
//        WxMenuButton button31 = new WxMenuButton();
//        button31.setType(MenuButtonType.VIEW);
//        button31.setName("网络问诊");
//        button31.setUrl("http://m.yjy361.com//?source=ProvincialHospital&ThridAccount=o4PUluFIOC91Tet0HRVenTbdBTsc&_auth_token=3a72ac813737ddf0b0cfe500b447e710@35462&_auth_code=0&_auth_code_msg=&_auth_client_type=CustomerM&_auth_client_device_no=ee0465c2a2364c35879dd10d49f2d857");
//
//        WxMenuButton button32 = new WxMenuButton();
//        button32.setType(MenuButtonType.MEDIA_ID);
//        button32.setName("领劵购物");
//        button32.setMediaId("pCVqT6_unMJZD-uISk8-XItzojBrQ42pC5mZHSR4RNY");

//        WxMenuButton button33 = new WxMenuButton();
//        button33.setType(MenuButtonType.CLICK);
//        button33.setName("赞一下我们");
//        button33.setKey("V1001_GOOD");

//        button3.getSubButtons().add(button31);
//        button3.getSubButtons().add(button32);
//        button3.getSubButtons().add(button33);

        WxMenu menu = new WxMenu();
        WxMenuButton button1 = new WxMenuButton();
        button1.setType(MenuButtonType.VIEW);
        button1.setName("乡村生活");
//button1.setUrl("http://wx.enjoysala.top/api/bbs/wx/authuser/wx8d2c0b334bbcf667/getUserInfoByAuth?townId=511322");
        button1.setUrl(wx_domainurl + "/api/bbs/wx/authuser/" + appid + "/getUserInfoByAuth?townId=511322");
        WxMenuButton button2 = new WxMenuButton();
        button2.setType(MenuButtonType.VIEW);
        button2.setName("批发商城");
        button2.setUrl("http://sxy.lsqxd.net/app/index.php?i=13&c=entry&eid=265");
        WxMenuButton button3 = new WxMenuButton();
        button3.setName("便民服务");

        menu.getButtons().add(button1);
        menu.getButtons().add(button2);
        menu.getButtons().add(button3);

        WxMenuButton button31 = new WxMenuButton();
        button31.setType(MenuButtonType.VIEW);
        button31.setName("网络问诊");
        button31.setUrl("http://m.yjy361.com//?source=ProvincialHospital&ThridAccount=o4PUluFIOC91Tet0HRVenTbdBTsc&_auth_token=3a72ac813737ddf0b0cfe500b447e710@35462&_auth_code=0&_auth_code_msg=&_auth_client_type=CustomerM&_auth_client_device_no=ee0465c2a2364c35879dd10d49f2d857");

        WxMenuButton button32 = new WxMenuButton();
        button32.setType(MenuButtonType.MEDIA_ID);
        button32.setName("领劵购物");
        button32.setMediaId("pwu2Ymu2AWYKUSACai4Jl4gUv8ei13i7qoCux0fZt8I");

        button3.getSubButtons().add(button31);
        button3.getSubButtons().add(button32);

        return WxMpConfiguration.getMpServices().get(appid).getMenuService().menuCreate(menu);
    }

    /**
     * <pre>
     * 自定义菜单创建接口
     * 详情请见： https://mp.weixin.qq.com/wiki?t=resource/res_main&id=mp1421141013&token=&lang=zh_CN
     * 如果要创建个性化菜单，请设置matchrule属性
     * 详情请见：https://mp.weixin.qq.com/wiki?t=resource/res_main&id=mp1455782296&token=&lang=zh_CN
     * </pre>
     *
     * @param json
     * @return 如果是个性化菜单，则返回menuid，否则返回null
     */
    @GetMapping("/create/{json}")
    public String menuCreate(@PathVariable String appid, @PathVariable String json) throws WxErrorException {
        return WxMpConfiguration.getMpServices().get(appid).getMenuService().menuCreate(json);
    }

    /**
     * <pre>
     * 自定义菜单删除接口
     * 详情请见: https://mp.weixin.qq.com/wiki?t=resource/res_main&id=mp1421141015&token=&lang=zh_CN
     * </pre>
     */
    @GetMapping("/delete")
    public void menuDelete(@PathVariable String appid) throws WxErrorException {
        WxMpConfiguration.getMpServices().get(appid).getMenuService().menuDelete();
    }

    /**
     * <pre>
     * 删除个性化菜单接口
     * 详情请见: https://mp.weixin.qq.com/wiki?t=resource/res_main&id=mp1455782296&token=&lang=zh_CN
     * </pre>
     *
     * @param menuId 个性化菜单的menuid
     */
    @GetMapping("/delete/{menuId}")
    public void menuDelete(@PathVariable String appid, @PathVariable String menuId) throws WxErrorException {
        WxMpConfiguration.getMpServices().get(appid).getMenuService().menuDelete(menuId);
    }

    /**
     * <pre>
     * 自定义菜单查询接口
     * 详情请见： https://mp.weixin.qq.com/wiki?t=resource/res_main&id=mp1421141014&token=&lang=zh_CN
     * </pre>
     */
    @GetMapping("/get")
    public WxMpMenu menuGet(@PathVariable String appid) throws WxErrorException {
        return WxMpConfiguration.getMpServices().get(appid).getMenuService().menuGet();
    }

    /**
     * <pre>
     * 测试个性化菜单匹配结果
     * 详情请见: http://mp.weixin.qq.com/wiki/0/c48ccd12b69ae023159b4bfaa7c39c20.html
     * </pre>
     *
     * @param userid 可以是粉丝的OpenID，也可以是粉丝的微信号。
     */
    @GetMapping("/menuTryMatch/{userid}")
    public WxMenu menuTryMatch(@PathVariable String appid, @PathVariable String userid) throws WxErrorException {
        return WxMpConfiguration.getMpServices().get(appid).getMenuService().menuTryMatch(userid);
    }

    /**
     * <pre>
     * 获取自定义菜单配置接口
     * 本接口将会提供公众号当前使用的自定义菜单的配置，如果公众号是通过API调用设置的菜单，则返回菜单的开发配置，而如果公众号是在公众平台官网通过网站功能发布菜单，则本接口返回运营者设置的菜单配置。
     * 请注意：
     * 1、第三方平台开发者可以通过本接口，在旗下公众号将业务授权给你后，立即通过本接口检测公众号的自定义菜单配置，并通过接口再次给公众号设置好自动回复规则，以提升公众号运营者的业务体验。
     * 2、本接口与自定义菜单查询接口的不同之处在于，本接口无论公众号的接口是如何设置的，都能查询到接口，而自定义菜单查询接口则仅能查询到使用API设置的菜单配置。
     * 3、认证/未认证的服务号/订阅号，以及接口测试号，均拥有该接口权限。
     * 4、从第三方平台的公众号登录授权机制上来说，该接口从属于消息与菜单权限集。
     * 5、本接口中返回的图片/语音/视频为临时素材（临时素材每次获取都不同，3天内有效，通过素材管理-获取临时素材接口来获取这些素材），本接口返回的图文消息为永久素材素材（通过素材管理-获取永久素材接口来获取这些素材）。
     *  接口调用请求说明:
     * http请求方式: GET（请使用https协议）
     * https://api.weixin.qq.com/cgi-bin/get_current_selfmenu_info?access_token=ACCESS_TOKEN
     * </pre>
     */
    @GetMapping("/getSelfMenuInfo")
    public WxMpGetSelfMenuInfoResult getSelfMenuInfo(@PathVariable String appid) throws WxErrorException {
        return WxMpConfiguration.getMpServices().get(appid).getMenuService().getSelfMenuInfo();
    }
}
