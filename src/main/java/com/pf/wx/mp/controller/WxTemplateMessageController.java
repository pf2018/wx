package com.pf.wx.mp.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.pf.wx.mp.config.WxMpConfiguration;

import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.api.WxMpTemplateMsgService;
import me.chanjar.weixin.mp.bean.template.WxMpTemplate;
import me.chanjar.weixin.mp.bean.template.WxMpTemplateData;
import me.chanjar.weixin.mp.bean.template.WxMpTemplateMessage;

@Controller
@RequestMapping("/api/bbs/wx/template/{appid}")
public class WxTemplateMessageController {

    @GetMapping("/sendTempMessage")
    public String getAuthUrlAndUserInfo(@PathVariable String appid, String open_id, String url) {
        WxMpService wxMpService = WxMpConfiguration.getMpServices().get(appid);
        try {
            WxMpTemplateMsgService wxMpTemplateMsgService = wxMpService.getTemplateMsgService();
            List<WxMpTemplate> templateList = wxMpTemplateMsgService.getAllPrivateTemplate();
            WxMpTemplate template = null;
            //  title 中含有  认证审核通过  关键字
            for (int i = 0; i < templateList.size(); i++) {
                String title = templateList.get(i).getTitle();
                if (title.indexOf("认证") != -1 && title.indexOf("通过") != -1) {
                    template = templateList.get(i);
                    break;
                }
            }
            if (template == null) {
                return "404";
            }
            WxMpTemplateMessage wxMpTemplateMessage = new WxMpTemplateMessage();
            wxMpTemplateMessage.setToUser(open_id);
            wxMpTemplateMessage.setTemplateId(template.getTemplateId());
            if (!StringUtils.isEmpty(url)) {
                wxMpTemplateMessage.setUrl(url);
            }
            String rest = wxMpTemplateMsgService.sendTemplateMsg(wxMpTemplateMessage);

            System.out.println(rest);
            return rest;
        } catch (WxErrorException e) {
            e.printStackTrace();
        }
        return "500";
    }

    @PostMapping("/sendTempMessageById")
    @ResponseBody
    public Map getAuthUrlAndUserInfoByIdPost(@PathVariable String appid,
                                                @RequestBody JSONObject param) {
    	if(param == null) {
    		return null;
    	}
    	String tmp_id = param.getString("tmp_id");
    	String url = param.getString("url");
    	JSONArray ja = param.getJSONArray("open_id");
    	
    	JSONObject data = param.getJSONObject("data");
    	
    	Map<String,String> m = new HashMap<String,String>();
        WxMpService wxMpService = WxMpConfiguration.getMpServices().get(appid);
        Map<String, Object> result = new HashMap();
        result.put("appid", appid);
        result.put("tmp_id", tmp_id);
        result.put("tmpurl_id", url);
        try {
            WxMpTemplateMsgService wxMpTemplateMsgService = wxMpService.getTemplateMsgService();

            List<Map> rests = new ArrayList<>();
            
            for (int i = 0; i < ja.size(); i++) {
                Map<String, Object> sendMsg = new HashMap();
                try {
                    WxMpTemplateMessage wxMpTemplateMessage = sendTempMsg(ja.getString(i), tmp_id, url, data);
                    String rest = wxMpTemplateMsgService.sendTemplateMsg(wxMpTemplateMessage);
                    sendMsg.put("code", 1);
                    sendMsg.put("end_data", rest);
                } catch (WxErrorException e) {
                    sendMsg.put("code", -1);
                    sendMsg.put("error_msg", e.getError());
                }
                sendMsg.put("open_id", ja.get(i));
                rests.add(sendMsg);
            }



            result.put("code", "200");
            result.put("data", rests);
        } catch (Exception e) {
            e.printStackTrace();
            result.put("code", "500");
        }
        return result;
    }

    private WxMpTemplateMessage sendTempMsg(String open_id, String tmp_id, String url, JSONObject data) {
        WxMpTemplateMessage wxMpTemplateMessage = new WxMpTemplateMessage();
        wxMpTemplateMessage.setToUser(open_id);
        wxMpTemplateMessage.setTemplateId(tmp_id);
        if (!StringUtils.isEmpty(url)) {
            wxMpTemplateMessage.setUrl(url);
        }

        if (null != data && !data.isEmpty()) {
            ArrayList<WxMpTemplateData> list = new ArrayList();
            for (String key:data.keySet()) {
                WxMpTemplateData templateData = new WxMpTemplateData();
                templateData.setName(key);
                templateData.setValue(data.getString(key));
                list.add(templateData);
            }
            wxMpTemplateMessage.setData(list);
        }

        return wxMpTemplateMessage;

    }
}
